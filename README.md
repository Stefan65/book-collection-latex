## Digitalisierte Bücher

Dies ist eine Sammlung digitalisierter Versionen älterer Bücher - aktuell hauptsächlich Kochbücher - in Form von LaTeX-basierten Dokumenten.

Das Ziel ist es, den Originaltext ohne Veränderungen beizubehalten, d.h. lediglich die Formatierung sollte möglicherweise abweichen.

### Aktueller Stand

Aktuell enthalten sind:

* `armster1840`: *Neues auf vieljährige praktische Erfahrung gegründetes Kochbuch* von Sophie Armster (5. Auflage, 1840)
* `hannemann1925`: *Kochbuch* von Elise Hannemann (81.-90. Auflage, 1925)
* `keyser1797-teil1`: *Das große Thüringisch-Erfurtische Kochbuch, Erster Band* ungenannten Autors (1797).
* `keyser1798-teil2`: *Das große Thüringisch-Erfurtische Kochbuch, Zweiter Band* ungenannten Autors (1798).
* `scheibler1823`: *Allgemeines Deutsches Kochbuch für bürgerliche Haushaltungen* von Sophie Wilhelmine Scheibler (5. Auflage, 1823).
* `scheibler1845`: *Allgemeines Deutsches Kochbuch für bürgerliche Haushaltungen* von Sophie Wilhelmine Scheibler (11. Auflage, 1845).
* `scheibler1845-teil2`: *Allgemeines Deutsches Kochbuch für bürgerliche Haushaltungen; Zweiter Theil* von Sopie Wilhelmine Scheibler (4. Auflage, 1845).
* `schicht`: *Schicht's Kochbuch* (5 Bände, 1928-1931).
* `singstock1819-teil1`: *Neuestes vollständigstes Handbuch der feinen Kochkunst; Erster Theil* von G. E. Singstock (2. Auflage, 1819).
* `singstock1819-teil2`: *Neuestes vollständigstes Handbuch der feinen Kochkunst; Zweiter Theil* von G. E. Singstock (2. Auflage, 1819).
* `singstock1819-teil3`: *Neuestes vollständigstes Handbuch der feinen Kochkunst; Dritter Theil* von G. E. Singstock (2. Auflage, 1819).

### Disclaimer

1. Ich übernehme keinerlei Gewähr für Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten LaTeX-Versionen.
2. Die von mir bereitgestellten LaTeX-Versionen von Schriftwerken sind nach bestem Wissen und Gewissen auf ihren Status als gemeinfrei überprüft worden. Eine durch mich erfolgte Bezeichnung einer LaTeX-Version als gemeinfrei ist nicht rechtsverbindlich. Die Bereitstellung einer LaTeX-Version ist nicht als konkludente Erklärung zu verstehen, dass das Schriftwerk gemeinfrei ist.
3. &nbsp;
    1. Ich hafte für Vorsatz und grobe Fahrlässigkeit.
    2. Sollte eine LaTeX-Version eines nicht gemeinfreien und urheberrechtlich geschützten Schriftwerkes aufgrund leichter Fahrlässigkeit von mir vervielfältigt oder öffentlich wiedergegeben worden sein, hafte ich nicht für die weiteren Verletzungshandlungen, die dadurch entstehen können, dass die Nutzer meiner LaTeX-Versionen diese - mit Ausnahme der gesetzlich erlaubten Fälle - vervielfältigen, verbreiten oder ihrerseits öffentlich wiedergeben.
    3. Für Schäden, die durch fehlerhafte Informationen zu einer LaTeX-Version entstanden sind, übernehme ich keine Haftung.
    4. Die vorstehenden Haftungsausschlüsse gelten nicht bei Verletzung von Leben, Körper und Gesundheit.
4. Die angebotenen LaTeX-Versionen sind eine Sammlung von Schriftwerken verschiedener Autoren. Der Inhalt der Schriften spiegelt nicht unbedingt meine persönliche Meinung wider.

### Unterstützung

Die Erstellung der Dateien erfolgt nach bestem Wissen und Gewissen. Es gibt allerdings keinen vollumfänglichen zweiten Durchlauf, um Tippfehler bzw. auch allgemeine Fehler in der Übertragung zu prüfen. Solltest Du daher auf eventuelle Ungereimtheiten, Tippfehler oder andere Probleme aufmerksam werden, so erstelle gerne einen entsprechenden Issue bzw. einen Merge Request. Um dem Original weitestgehend treu zu bleiben, wurden und werden allerdings keine Fehler korrigiert, die bereits im Original existierten.[^1]

Jeder Unterordner führt im `README` auch Hinweise dazu auf, worauf die vorliegende Version basiert. Dies sind im Regelfall kostenfrei verfügbare Digitalisate, welche zur Überprüfung verwendet werden können.

Weitere mögliche Verbesserungen:

* Verbesserung des Layouts der Titelseiten
* Überführung der Dokumente in ein sinnvolles zweiseitiges Layout
* Korrektur der Ligaturen im Sperrsatz
* Korrektur fehlerhafter/fehlender Zeilenumbrüche
* Behebung von Warnungen aus dem Log
* ...

#### Erstellung der PDF-Dateien

Um die PDFs erstellen zu können, ist eine LaTeX-Installation notwendig. Die jeweiligen READMEs geben die notwendigen Befehle an, um im speziellen Fall die PDF-Dateien zu erstellen. Damit diese verwendet werden können, ist zusätzlich `make` notwendig.

### Weiterverwendung

Für die hier vertretenen Werke ist das Urheberrecht abgelaufen, weshalb die Texte an sich mittlerweile unter Public Domain stehen. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.

Alle Teile, die nicht als gemeinfrei einzustufen sind, stehen unter [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.de).

Ich freue mich über Hinweise bei Verwendung zur Aufrechterhaltung meiner Motivation an diesem mehr oder minder aufwändigen Freizeitprojekt. Zudem begrüße ich Korrekturen, um damit zunehmend die Qualität der Quelldaten zu steigern.

### Fragen und Antworten

#### Warum LaTeX?

* LaTeX-Quellcode ist Plaintext und lässt sich daher ohne spezielle Software bearbeiten, was eine freie Wahl des Editors zulässt.
* Die Ausgabe ist im Standardfall PDF, womit heutzutage eigentlich jeder etwas anfangen kann und das auch eine breite Softwareunterstützung bietet.
* Anders als beispielsweise bei Markdown oder wirklichem Plaintext sind die Formatierungsmöglichkeiten nicht begrenzt, während der Export in diese Formate allerdings durch Tools wie Pandoc oder ähnliches jederzeit möglich ist.

#### Reichen die originalen Digitalisate nicht aus?

Das kommt sicherlich auf den Verwendungszweck an. Ein Teil der verfügbaren Digitalisate ist lediglich gescannt und nicht weiter in der Darstellung optimiert bzw. für eine sinnvolle Verwendung einem (ausreichenden) Postprocessing unterworfen worden. Zudem ist häufig - wenn überhaupt - der Textinhalt nur mittels OCR verfügbar, was erfahrungsgemäß nicht immer von bester Qualität sein muss (wobei teilweise auch keine Möglichkeit der Korrektur gegeben ist). Häufig handelt es sich durch die Scans auch um recht große Datenmengen.

Um eine Durchsuchbarkeit und möglichst gute Weiterverarbeitung zu ermöglichen, ist daher die manuelle Nachbearbeitung wie in diesem Projekt beinahe unabdingbar.

#### Wo sind die Bilder/Grafiken aus den Werken?

Jegliche Bilder/Grafiken, die Teil der Originalwerke waren, werden aktuell ausgelassen. Dies liegt vor allem daran, dass die meisten Digitalisate, die ich als Grundlage für die Übertragung verwende, nicht von mir selbst erstellt wurden.

Während die Texte selbst gemeinfrei sind, trifft dies in der Regel nicht auf die Digitalisate/Scans an sich zu - die Nutzungsbedingungen für die Weiterverwendung der Scans sind restriktiver und verlangen für einige Anwendungsfälle den Erwerb kostenpflichtiger Lizenzen. Zudem fehlen teilweise Grafiken bzw. liegen jene lediglich in moderater Qualität vor.

#### Warum gibt es in diesem Repository nur die LaTeX-Quelltexte?

Die LaTeX-Quelltexte sind am universalsten einsetzbar und können mit einer eigenen lokalen LaTeX-Installation (oder einem Online-Tool) selbst in PDF-Dateien überführt werden. Ein Tracking der erstellten PDF-Dateien via Git halte ich aufgrund des Binärdatei-Charakters nicht für sinvoll.

#### Welche Software empfiehlt sich für die Digitalisierung?

Das kommt sicherlich auf die persönlichen Präferenzen und vorhandenen Vorkenntnisse an. Für das Digitalisat von *Schicht's Kochbuch* habe ich mit dem folgenden Ansatz (auf einem Ubuntu-System) gute Erfahrungen gemacht:

1. Scan des Originals mittels [XSane](https://gitlab.com/sane-project/frontend/xsane) als TIFF-Datei mit 600 dpi im Farbmodus. (Zur Vereinfachung eignet sich möglicherweise ein einfacher USB-Fußschalter, der auf <kbd>Ctrl + Return</kbd> gemappt ist und somit ein handfreies Starten des Scanvorgangs ermöglicht.)
2. Aufbereitung der Scans mittels [ScanTailor](http://scantailor.org/) bzw. einem der weiterhin aktualisierten Forks.
3. Erstellung einer DJVU-Datei mittels [didjvu](https://github.com/jwilk/didjvu).
4. Erstellung eines OCR-Layers mittels [ocrodjvu](https://github.com/jwilk/ocrodjvu) unter Verwendung der Tesseract-Engine.

#### Why is this project only available in German?

All of the books available here are written in German, so it probably does not make sense to keep the descriptions in English as well. Nevertheless, some of the documentation inside the LaTeX code or the commit messages may be written in English.


[^1]: Getätigte Korrekturen sind im Quelltext angegeben. Diese basieren im Regelfall auf den veröffentlichten Errata (z.B. bei *Schicht's Kochbuch*) oder beziehen sich auf fehlerhafte Rezeptreferenzen.
