## Das große Thüringisch-Erfurtische Kochbuch, Zweiter Band (1798)

Dies ist eine digitalisierte Version des zweiten Bands des "Großen Thüringisch-Erfurtischen Kochbuchs" ungenannten Autors aus dem Jahre 1798.

Die vorliegenden Dateien basieren größtenteils auf dem Digitalisat der Herzogin Anna Amalia Bibliothek/Klassik Stiftung Weimar, welches unter https://haab-digital.klassik-stiftung.de/viewer/resolver?urn=urn:nbn:de:gbv:32-1-10028124206 abrufbar ist. Druckfehler von Seite 382 des Digitalisats wurden bereits integriert.

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Um bei einer Verwendung dieser Version Übertragungsfehler auszuschließen, sollte ein entsprechender Abgleich mit dem (kostenfrei verfügbaren) Digitalisat erfolgen.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `keyser1798-teil2.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `keyser1798-teil2_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

Die verwendete Auflage stammt aus dem Jahre 1798, es ist kein Autor namentlich aufgeführt. Das Erscheinungsjahr liegt definitiv über 70 Jahre zurück. Somit ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
