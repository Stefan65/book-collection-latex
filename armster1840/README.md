## Neues auf vieljährige praktische Erfahrung gegründetes Kochbuch (1840)

Dies ist eine digitalisierte Version der fünften Auflage des "Neuen auf vieljähriger praktischer Erfahrung gegründeten Kochbuchs" von Sophie Armster aus dem Jahre 1840.

Die vorliegenden Dateien basieren auf dem Digitalisat eines Buches aus der New York Public Library, welches durch Google digitalisiert wurde und unter https://hdl.handle.net/2027/nyp.33433017316534 abrufbar ist.

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Um bei einer Verwendung dieser Version Übertragungsfehler auszuschließen, sollte ein entsprechender Abgleich mit dem (kostenfrei verfügbaren) Digitalisat erfolgen.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `armster1840.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `armster1840_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

Die verwendete Auflage stammt aus dem Jahre 1840. Die Autorin ist daher auf jeden Fall bereits seit über 70 Jahren tot ([Wikipedia](https://nds.wikipedia.org/wiki/Sophie_Armster) führt 1832 auf). Somit ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
