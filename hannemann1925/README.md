## Kochbuch (1925)

Dies ist eine digitalisierte Version des Kochbuchs von Elise Hannemann aus dem Jahre 1925.

Die vorliegenden Dateien basieren auf dem Originaldruck des Werkes.

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Meines Wissens existiert aktuell allerdings kein öffentlich verfügbares Digitalisat, welches einen selbstständigen Abgleich ermöglichen würde. Die Veröffentlichung einer entsprechenden DJVU-Datei erfolgt möglicherweise zu einem späteren Zeitpunkt.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `hannemann1925.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `hannemann1925_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

In der verwendeten Ausgabe ist nur Elise Hannemann als Autorin genannt, die im Jahre 1934 starb (vgl. [Wikipedia](https://de.wikipedia.org/wiki/Elise_Hannemann). Somit ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
