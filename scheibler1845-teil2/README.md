## Allgemeines Deutsches Kochbuch, Zweiter Teil (1845)

Dies ist eine digitalisierte Version der vierten Auflage des zweiten Teils des "Allgemeinen Deutschen Kochbuchs für bürgerliche Haushaltungen" von Sophie Wilhelmine Scheibler aus dem Jahre 1845.

Die vorliegenden Dateien basieren größtenteils auf dem Digitalisat der Staatlichen Bibliothek Regensburg, welches unter http://mdz-nbn-resolving.de/urn:nbn:de:bvb:12-bsb11302157-7 abrufbar ist.

Im finalen Dokument fehlen die beiden Kupfertafeln vom Ende des Buches mit zusätzlichen Abbildungen, da deren Erwerb und Verwendung für mich kostenpflichtig ist bzw. wäre. Mittels des [Digitalisierungsformulars](https://www.staatliche-bibliothek-regensburg.de/benutzung-service/reproduktion/) der Staatlichen Bibliothek Regensburg ist ein Erwerb von Scans dieser Seiten für private und nicht-kommerzielle Nutzung gegen ein Entgelt (ca. 7.50 €, Stand März 2019) möglich ([Katalogeintrag des Buches](https://www.regensburger-katalog.de/s/ubrsbr/de/2/1035/BV040922152)).

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Um bei einer Verwendung dieser Version Übertragungsfehler auszuschließen, sollte ein entsprechender Abgleich mit dem (kostenfrei verfügbaren) Digitalisat erfolgen.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `scheibler1845-teil2.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `scheibler1845-teil2_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

Die verwendete Auflage stammt aus dem Jahre 1845. Die angegebene Autorin hat im Erscheinungsjahr laut [Wikipedia](https://de.wikipedia.org/wiki/Sophie_Wilhelmine_Scheibler) bereits nicht mehr gelebt. Unter Beachtung des Erscheinungsjahrs ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
