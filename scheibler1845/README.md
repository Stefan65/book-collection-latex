## Allgemeines Deutsches Kochbuch (1845)

Dies ist eine digitalisierte Version der elften Auflage des "Allgemeinen Deutschen Kochbuchs für bürgerliche Haushaltungen" von Sophie Wilhelmine Scheibler aus dem Jahre 1845.

Die vorliegenden Dateien basieren größtenteils auf dem Digitalisat der Staatlichen Bibliothek Regensburg, welches unter http://mdz-nbn-resolving.de/urn:nbn:de:bvb:12-bsb11302156-2 abrufbar ist.

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Um bei einer Verwendung dieser Version Übertragungsfehler auszuschließen, sollte ein entsprechender Abgleich mit dem (kostenfrei verfügbaren) Digitalisat erfolgen.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `scheibler1845.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `scheibler1845_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

Die verwendete Auflage stammt aus dem Jahre 1845. Die angegebene Autorin hat im Erscheinungsjahr laut [Wikipedia](https://de.wikipedia.org/wiki/Sophie_Wilhelmine_Scheibler) bereits nicht mehr gelebt. Unter Beachtung des Erscheinungsjahrs ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
