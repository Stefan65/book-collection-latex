## Schicht's Kochbuch

Dies ist eine digitalisierte Version der fünf Bände von "Schicht's Kochbuch" aus den Jahren 1928-1931.

Die vorliegenden Dateien basieren auf den Originaldrucken des Gesamtwerkes inklusive der Richtigstellungen zu Band 1, die im verwendeten Exemplar auf der Titelseite eingeklebt sind.

In der vorliegenden Version wurden alle Abbildungen zunächst ausgelassen.

Für den ersten Band ist ein kostenfreies Digitalisat von Andreas Romeyke verfügbar (siehe https://art1pirat.blogspot.com/2016/12/schichts-kochbuch-bd-1.html), für alle anderen Bände folgt eine Veröffentlichung entsprechender DJVU-Dateien möglicherweise zu einem späteren Zeitpunkt.

### Erstellung der PDF-Dateien

`schicht-bandX.tex` enthält jeweils das gesamte Dokument für den entsprechenden Band `X`. Um alle Bände zu setzen, kann `make all` verwendet werden, `make band1` setzt dagegen beispielsweise nur den ersten Band.

### Weiterverwendung

Das Gesamtwerk stammt laut der [Datenbank der DNB](https://portal.dnb.de/opac.htm?method=simpleSearch&reset=true&cqlMode=true&query=partOf%3D56088897X&selectedCategory=any) aus der Zeit bis 1931. Es ist kein Autor angegeben, weshalb das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei sein sollte. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
