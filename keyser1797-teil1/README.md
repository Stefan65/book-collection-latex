## Das große Thüringisch-Erfurtische Kochbuch, Erster Band (1797)

Dies ist eine digitalisierte Version des ersten Bands des "Großen Thüringisch-Erfurtischen Kochbuchs" ungenannten Autors aus dem Jahre 1797.

Die vorliegenden Dateien basieren größtenteils auf dem Digitalisat der Herzogin Anna Amalia Bibliothek/Klassik Stiftung Weimar, welches unter https://haab-digital.klassik-stiftung.de/viewer/resolver?urn=urn:nbn:de:gbv:32-1-10028141182 abrufbar ist. Druckfehler und Verbesserungen von Seite 477 des Digitalisats wurden bereits integriert.

Die Erstellung des Dokuments erfolgte nach bestem Wissen und Gewissen. Es wird keine Gewähr für Richtigkeit übernommen. Um bei einer Verwendung dieser Version Übertragungsfehler auszuschließen, sollte ein entsprechender Abgleich mit dem (kostenfrei verfügbaren) Digitalisat erfolgen.

Leider fehlen im verwendeten Digitalisat die Seiten 317/318, da diese herausgetrennt wurden, siehe auch https://lhwei.gbv.de/DB=2/XMLPRS=N/PPN?PPN=610158198. Dadurch fehlen der zweite Teil von Rezept 545, die gesamten Rezepte 546 und 547 sowie der erste Teil von Rezept 548. Sollte jemand Zugriff auf ein Exemplar haben, in welchem diese Seiten nicht fehlen, würde ich mich über einen entsprechenden Scan und/oder eine entsprechende Ergänzung freuen.

### Erstellung der PDF-Dateien

`main.tex` enthält den eigentlichen Inhalt des Buches. Es existieren zwei verschiedene Hauptdokumente, die beide auf `main.tex` zugreifen:

* `keyser1797-teil1.tex` erstellt ein Dokument mit Frakturschrift, ähnlich zum Original. Das Dokument lässt sich mittels `make fraktur` erstellen. Hierfür wird standardmäßig PdfLaTeX verwendet.
* `keyser1797-teil1_noFraktur.tex` erstellt ein Dokument ohne Fraktur mit TeX Gyre Pagella. Das Dokument lässt sich mittels `make normal` erstellen. Hierfür wird standardmäßig LuaLaTeX verwendet, um die notwendige automatische Ersetzung im Fließtext durchzuführen. (Es ist möglicherweise notwendig, den korrekten Fontnamen für TeX Gyre Pagella zu setzen, da dieser je nach System unterschiedlich zu sein scheint und aktuell möglicherweise nicht alle Fälle korrekt abgedeckt werden.)

Falls die Erstellung eines Index für alle Rezepte am Ende des Dokuments gewünscht ist, so kann dieser in Zeile 12 von `main.tex` aktiviert werden. Die entsprechenden Make-Targets sind dann `frakturIndex` und `normalIndex`.

### Weiterverwendung

Die verwendete Auflage stammt aus dem Jahre 1797, es ist kein Autor namentlich aufgeführt. Das Erscheinungsjahr liegt definitiv über 70 Jahre zurück. Somit ist das Urheberrecht entsprechend abgelaufen und das Werk selbst gemeinfrei. Dies ist keine rechtsverbindliche Angabe - im Zweifel unbedingt entsprechend eigene Nachforschungen anstellen und die Grenzen des Erlaubten prüfen.
